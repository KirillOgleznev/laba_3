/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QLabel *label_8;
    QPushButton *pushButton_2;
    QPushButton *pushButton_6;
    QTextBrowser *textBrowser_2;
    QLineEdit *lineEdit_5;
    QLineEdit *lineEdit;
    QPushButton *pushButton_3;
    QLineEdit *lineEdit_3;
    QLabel *label_5;
    QLabel *label_3;
    QTextBrowser *textBrowser_6;
    QLineEdit *lineEdit_2;
    QLabel *label;
    QTextBrowser *textBrowser_5;
    QTextBrowser *textBrowser_3;
    QLabel *label_10;
    QLineEdit *lineEdit_4;
    QLabel *label_4;
    QPushButton *pushButton_5;
    QTextBrowser *textBrowser_4;
    QTextBrowser *textBrowser;
    QLabel *label_7;
    QPushButton *pushButton_4;
    QLabel *label_9;
    QLabel *label_6;
    QPushButton *pushButton;
    QLabel *label_11;
    QLabel *label_2;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1107, 312);
        MainWindow->setAutoFillBackground(true);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setAutoFillBackground(true);

        gridLayout->addWidget(label_8, 2, 12, 1, 1);

        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setAutoFillBackground(true);

        gridLayout->addWidget(pushButton_2, 2, 4, 1, 1);

        pushButton_6 = new QPushButton(centralWidget);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));
        pushButton_6->setAutoFillBackground(true);

        gridLayout->addWidget(pushButton_6, 2, 13, 1, 2);

        textBrowser_2 = new QTextBrowser(centralWidget);
        textBrowser_2->setObjectName(QStringLiteral("textBrowser_2"));
        textBrowser_2->setAutoFillBackground(true);

        gridLayout->addWidget(textBrowser_2, 3, 2, 1, 3);

        lineEdit_5 = new QLineEdit(centralWidget);
        lineEdit_5->setObjectName(QStringLiteral("lineEdit_5"));

        gridLayout->addWidget(lineEdit_5, 1, 0, 1, 4);

        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setAutoFillBackground(true);

        gridLayout->addWidget(lineEdit, 1, 6, 1, 3);

        pushButton_3 = new QPushButton(centralWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setAutoFillBackground(true);

        gridLayout->addWidget(pushButton_3, 2, 6, 1, 1);

        lineEdit_3 = new QLineEdit(centralWidget);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));
        lineEdit_3->setAutoFillBackground(true);

        gridLayout->addWidget(lineEdit_3, 1, 9, 1, 5);

        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setAutoFillBackground(true);

        gridLayout->addWidget(label_5, 2, 5, 1, 1);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setAutoFillBackground(true);

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        textBrowser_6 = new QTextBrowser(centralWidget);
        textBrowser_6->setObjectName(QStringLiteral("textBrowser_6"));
        textBrowser_6->setAutoFillBackground(true);

        gridLayout->addWidget(textBrowser_6, 3, 13, 1, 2);

        lineEdit_2 = new QLineEdit(centralWidget);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setAutoFillBackground(true);

        gridLayout->addWidget(lineEdit_2, 1, 4, 1, 1);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setAutoFillBackground(true);

        gridLayout->addWidget(label, 0, 6, 1, 1);

        textBrowser_5 = new QTextBrowser(centralWidget);
        textBrowser_5->setObjectName(QStringLiteral("textBrowser_5"));
        textBrowser_5->setAutoFillBackground(true);

        gridLayout->addWidget(textBrowser_5, 3, 11, 1, 1);

        textBrowser_3 = new QTextBrowser(centralWidget);
        textBrowser_3->setObjectName(QStringLiteral("textBrowser_3"));
        textBrowser_3->setAutoFillBackground(true);

        gridLayout->addWidget(textBrowser_3, 3, 5, 1, 2);

        label_10 = new QLabel(centralWidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setAutoFillBackground(true);

        gridLayout->addWidget(label_10, 0, 14, 1, 1);

        lineEdit_4 = new QLineEdit(centralWidget);
        lineEdit_4->setObjectName(QStringLiteral("lineEdit_4"));
        lineEdit_4->setAutoFillBackground(true);

        gridLayout->addWidget(lineEdit_4, 1, 14, 1, 1);

        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout->addWidget(label_4, 2, 2, 1, 1);

        pushButton_5 = new QPushButton(centralWidget);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));
        pushButton_5->setAutoFillBackground(true);

        gridLayout->addWidget(pushButton_5, 2, 11, 1, 1);

        textBrowser_4 = new QTextBrowser(centralWidget);
        textBrowser_4->setObjectName(QStringLiteral("textBrowser_4"));
        textBrowser_4->setAutoFillBackground(true);

        gridLayout->addWidget(textBrowser_4, 3, 8, 1, 2);

        textBrowser = new QTextBrowser(centralWidget);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));
        textBrowser->setAutoFillBackground(true);

        gridLayout->addWidget(textBrowser, 3, 0, 1, 2);

        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setAutoFillBackground(true);

        gridLayout->addWidget(label_7, 2, 10, 1, 1);

        pushButton_4 = new QPushButton(centralWidget);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        pushButton_4->setAutoFillBackground(true);

        gridLayout->addWidget(pushButton_4, 2, 8, 1, 2);

        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setAutoFillBackground(true);

        gridLayout->addWidget(label_9, 0, 11, 1, 1);

        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setAutoFillBackground(true);

        gridLayout->addWidget(label_6, 2, 7, 1, 1);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setAutoFillBackground(true);

        gridLayout->addWidget(pushButton, 2, 1, 1, 1);

        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QStringLiteral("label_11"));

        gridLayout->addWidget(label_11, 0, 1, 1, 1);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setAutoFillBackground(true);

        gridLayout->addWidget(label_2, 0, 4, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1107, 25));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "6)", nullptr));
        pushButton_2->setText(QApplication::translate("MainWindow", "\320\237\320\260\320\273\320\270\320\275\320\264\321\200\320\276\320\274", nullptr));
        pushButton_6->setText(QApplication::translate("MainWindow", "\320\236\320\272\321\200\321\203\320\263\320\273\320\265\320\275\320\270\320\265 \321\207\320\270\321\201\320\273\320\260", nullptr));
        pushButton_3->setText(QApplication::translate("MainWindow", "\320\232\320\276\320\273\320\270\321\207\320\265\321\201\321\202\320\262\320\276 \320\263\320\273\320\260\321\201\320\275\321\213\321\205", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "3)", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "1)", nullptr));
        label->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\321\201\321\202\320\270 \321\201\321\202\321\200\320\276\320\272\321\203 \320\272\320\270\321\200\320\270\320\273\320\273\320\270\321\206\320\265\320\271 (2,3,4,5)", nullptr));
        label_10->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\321\201\321\202\320\270 K (6)", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "2)", nullptr));
        pushButton_5->setText(QApplication::translate("MainWindow", "\320\232\320\276\320\273\320\270\321\207\320\265\321\201\321\202\320\262\320\276 \320\261\321\203\320\272\320\262", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "5)", nullptr));
        pushButton_4->setText(QApplication::translate("MainWindow", "\320\232\320\276\320\273\320\270\321\207\320\265\321\201\321\202\320\262\320\276 \321\201\320\276\320\263\320\273\320\260\321\201\320\275\321\213\321\205", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\321\201\321\202\320\270 N (6)", nullptr));
        label_6->setText(QApplication::translate("MainWindow", "4)", nullptr));
        pushButton->setText(QApplication::translate("MainWindow", "\320\220\320\273\320\263\320\276\321\200\320\270\321\202\320\274 \320\246\320\265\320\267\320\260\321\200\321\217", nullptr));
        label_11->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \321\201\321\202\321\200\320\276\320\272\321\203 \320\273\320\260\321\202\320\270\320\275\320\270\321\206\320\265\320\271 (1)", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "\320\222\320\262\320\265\321\201\321\202\320\270 U (1)", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
